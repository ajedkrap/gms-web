import React from 'react'
import { useSelector } from 'react-redux'
import { BrowserRouter, Switch, Route } from 'react-router-dom'


import Login from "./pages/login"
import Home from "./pages/home"

const App = () => {
  const officer = useSelector(state => state.officer);
  console.log(officer)

  return (

    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/home" exact component={Home} />
      </Switch>
    </BrowserRouter>

  )
}

export default App;
