

const Columns = [
  {
    title: "Management",
    columns: [
      {
        title: "officer",
        sub: [
          {
            label: "list officer",
            path: "/officer/list"
          },
          {
            label: "tambah officer",
            path: "/officer/new"
          }
        ]
      },
      {
        title: "anggota",
        sub: [
          {
            label: "list anggota",
            path: "/client/list"
          }
        ]
      },
      {
        title: "simpanan",
        sub: [
          {
            label: "list simpanan",
            path: "/deposit/list"
          }
        ]
      },
      {
        title: "pinjaman",
        sub: [
          {
            label: "list pinjaman",
            path: "/loan/list"
          }
        ]
      },
    ]
  },
  {
    title: "operational",
    columns: [
      {
        title: "produk-simpanan",
        sub: [
          {
            label: 'list produk simpnanan',
            path: "/product/deposit/list"
          },
          {
            label: 'tambah produk simpanan',
            path: "/product/deposit/new"
          }
        ]
      },
      {
        title: "produk-pinjaman",
        sub: [
          {
            label: 'list produk pinjaman',
            path: "/product/loan/list"
          },
          {
            label: 'tambah produk pinjaman',
            path: "/product/loan/new"
          }
        ]
      },
      {
        title: "pengajuan",
        sub: [
          {
            label: 'list produk pinjaman',
            path: "/apply/deposit"
          },
          {
            label: 'tambah produk pinjaman',
            path: "/apply/loan"
          }
        ]
      },
    ]
  },
  {
    title: 'accounting',
    columns: [
      {
        title: 'transaction',
        sub: [
          {
            label: 'list transaction',
            path: '/transaction/list'
          },
          {
            label: 'tambah transaksi',
            path: '/transaction/list'
          }
        ]
      }
    ]
  }
]

export default Columns