const color = {
  bg1: '#F7F7F7',
  accent1: '#003459',
  white: '#FFFFFF',
  button: '#E1ECF4'
}

export default color